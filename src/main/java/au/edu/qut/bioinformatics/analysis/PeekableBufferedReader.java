/*
 * Copyright 2013 Darran Kartaschew.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package au.edu.qut.bioinformatics.analysis;

import java.io.IOException;
import java.io.Reader;

/**
 * Implement a Buffered Reader for ES Client.
 * <br>The default BufferedReader does not handle the input stream from ES
 * correct, thus have implemented a weight class to implement buffered reading.
 *
 * @author Darran Kartaschew
 */
public class PeekableBufferedReader {

    /**
     * The input stream to read from.
     */
    private final Reader inputStream;
    /**
     * The size of the read-ahead buffer to maintain.
     */
    private final int bufferSize;
    /**
     * The internal buffer to store characters into.
     */
    private final char[] buffer;
    /**
     * Pointer to the start of the buffer, the next byte to be read.
     */
    private int bufferStart = 0;
    /**
     * Pointer to the current byte past the last read byte in the buffer.
     */
    private int bufferEnd = 0;

    /**
     * Construct a new buffered reader.
     *
     * @param input The input stream to read characters from.
     * @param bufferSize The size of the buffer to maintain. (Note: all reads
     * requested MUST fit into this buffer).
     * @throws IOException If the reader is NULL or the buffer is less or equal
     * to zero.
     */
    public PeekableBufferedReader(Reader input, int bufferSize) throws IOException {
        if (input == null || bufferSize < 1) {
            throw new IOException("The reader is null or the buffer size is less or equal to zero");
        }
        this.inputStream = input;
        this.bufferSize = bufferSize;
        this.buffer = new char[bufferSize];
    }

    /**
     * Reads characters from the input stream and returns them in the array
     * provided.<br> This method does NOT increment the reader's position in the
     * stream.
     *
     * @param cbuf The buffer to place the characters into.
     * @param len The number of characters to read.
     * @return The number of characters read, or -1 for end for stream.
     * @throws IOException If an IO error occurs or the input request is larger
     * than the array provided.
     */
    public int read(char[] cbuf, int len) throws IOException {
        if (cbuf == null) {
            throw new IOException("The target buffer is NULL");
        }
        if (cbuf.length < len) {
            throw new IOException("The target buffer is smaller than the request size");
        }
        if (len < 0) {
            throw new IOException("The read size is negative");
        }
        if (len > buffer.length) {
            throw new IOException("The source/cache buffer is smaller than the request size");
        }
        if (len == 0) {
            return 0; // we read nothing!
        }
        // See if we have enough left in the buffer, otherwise just increment the pointers.
        if (len <= bufferEnd - bufferStart) {
            // Just copy the buffer to target.
            System.arraycopy(buffer, bufferStart, cbuf, 0, len);
            return len;
        }
        // Need to fill in the buffer before copying.
        // Shift the buffer and reset the bufferStart/bufferEnd values
        int startOfBuffer = 0;
        for (int index = bufferStart; index < bufferEnd; index++) {
            buffer[startOfBuffer++] = buffer[index];
        }
        bufferEnd -= bufferStart;
        bufferStart = 0;

        // Refill a complete buffer.
        int read = inputStream.read(buffer, bufferEnd, buffer.length - bufferEnd);
        if (read == -1) {
            if (bufferEnd != 0) {
                System.arraycopy(buffer, 0, cbuf, 0, bufferEnd);
                return bufferEnd;
            } else {
                return read;
            }
        }
        bufferEnd += read;
        System.arraycopy(buffer, 0, cbuf, 0, Math.min(bufferEnd, len));
        return Math.min(bufferEnd, len);
    }

    /**
     * Reads characters into a portion of an array. This method will block until
     * some input is available, an I/O error occurs, or the end of the stream is
     * reached.
     *
     * @param cbuf Destination buffer
     * @param off Offset at which to start storing characters
     * @param len Maximum number of characters to read
     *
     * @return The number of characters read, or -1 if the end of the stream has
     * been reached
     *
     * @exception IOException If an I/O error occurs
     */
    private int read(char cbuf[], int off, int len) throws IOException {
        if (cbuf == null) {
            throw new IOException("The target buffer is NULL");
        }
        if (len < 0) {
            throw new IOException("The read size is negative");
        }
        if (off < 0) {
            throw new IOException("The offset to write into the buffer is negative.");
        }
        if (cbuf.length < len + off) {
            throw new IOException("The target buffer is smaller than the request size");
        }
        if (len == 0) {
            return 0; // we read nothing!
        }
        int toRead = len;
        int element;
        // Continue to fill the buffer until we run out of input or the buffer is full.
        while(toRead > 0){
            element = inputStream.read();
            if(element == -1){
                // End of stream.
                if(toRead == len){
                    return -1; // Nothing read.
                } else {
                    return (len-toRead); // Amount actually read.
                }
            }
            cbuf[off++] = (char)(element & 0xff);
            toRead--;
        }
        return len;
    }

    /**
     * Seek forward in the stream by the number of specified characters.
     *
     * @param offset The number of bytes to seek forward.
     * @throws IOException If the seek on the stream fails.
     */
    public void seek(int offset) throws IOException {
        if (offset < 0) {
            throw new IOException("Unable to seek backwards in the reader");
        }
        if ((bufferEnd - bufferStart) - offset > 0) {
            bufferStart += offset;
        } else {
            //The buffer is now no longer valid.
            // see how much we have to skip
            int skip = offset - (bufferEnd - bufferStart);
            bufferStart = 0;
            bufferEnd = 0;
            inputStream.skip(skip);
        }
    }

    /**
     * Get the size of the underlying buffer.
     *
     * @return The size of the buffer in bytes.
     */
    public int getBufferSize() {
        return bufferSize;
    }
}
