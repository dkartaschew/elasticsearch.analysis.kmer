/*
 * Copyright 2013 Darran Kartaschew.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package au.edu.qut.bioinformatics.analysis;

import java.io.IOException;

import org.apache.lucene.analysis.Token;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.analysis.tokenattributes.TypeAttribute;

/**
 * Generic helper methods for reading tokens from a token stream.
 *
 * @author Darran Karatschew
 */
public class TokenUtils {

    /**
     * Get the next Token from the given token stream.
     *
     * @param input The token input stream
     * @param reusableToken A token that may be reused or NULL if a new token is to be created.
     * @return A token from the token stream.
     * @throws IOException If an IO exception occurred reading from the token stream.
     */
    public static Token nextToken(TokenStream input, Token reusableToken) throws IOException {
        if (input == null) {
            return null;
        }
        if (!input.incrementToken()) {
            return null;
        }

        CharTermAttribute termAtt = input.getAttribute(CharTermAttribute.class);
        OffsetAttribute offsetAtt = input.getAttribute(OffsetAttribute.class);
        TypeAttribute typeAtt = input.getAttribute(TypeAttribute.class);

        if (reusableToken == null) {
            reusableToken = new Token();
        }

        reusableToken.clear();
        if (termAtt != null) {
            reusableToken.copyBuffer(termAtt.buffer(), 0, termAtt.length());
        }
        if (offsetAtt != null) {
            reusableToken.setOffset(offsetAtt.startOffset(), offsetAtt.endOffset());
        }
        if (typeAtt != null) {
            reusableToken.setType(typeAtt.type());
        }

        return reusableToken;
    }
}
