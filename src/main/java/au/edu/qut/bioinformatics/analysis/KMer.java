/*
 * Copyright 2013 Darran Kartaschew.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package au.edu.qut.bioinformatics.analysis;

import au.edu.qut.bioinformatics.exceptions.SequenceException;
import java.util.Arrays;
import java.util.Objects;

/**
 * Class representing a raw kmer of length k.
 *
 * @author Darran Karatschew
 */
public class KMer {

    /**
     * The default word type for lucene.
     */
    public static final String TYPE_WORD = "word";
    /**
     * The raw kmer.
     */
    private final char[] kmer;
    /**
     * The offset of the kmer in the entire sequence. (This is the index value for lucene).
     */
    private final int offset;
    /**
     * The word type of the kmer.
     */
    private final String type;

    /**
     * Factory method to create a kmer from a much larger sequence.
     *
     * @param rawsequence The raw sequence to extract the kmer from.
     * @param sequenceStartOffset The starting offset into the raw sequence, which denotes the start of the kmer.
     * @param kmerLength The length of the kmer to extract.
     * @param readerIdx The offset of the kmer in the entire sequence. (This is the index value for lucene).
     * @param wordType The word type of the kmer.
     * @return A KMer.
     * @throws SequenceException If the sequence is null or has no length<br>The sequence offsets or length of the kmer
     * is negative, of the kmer specified extends passed the end of the sequence<br>The reader index (for lucene) is
     * negative<br>The word type for lucene is invalid.
     */
    public static KMer create(char[] rawsequence, int sequenceStartOffset, int kmerLength, int readerIdx, String wordType) throws SequenceException {
        if (rawsequence == null || rawsequence.length == 0) {
            throw new SequenceException("The raw sequence passed in is either NULL or is empty.");
        }
        if (kmerLength < 1 || sequenceStartOffset < 0 || sequenceStartOffset + kmerLength > rawsequence.length) {
            throw new SequenceException("The kmer offset or its length specified in less than 1, the sequence specified extends passed the end of the raw sequnce");
        }
        if (readerIdx < 0) {
            throw new SequenceException("The reader index for lucene is less than 0?");
        }
        if (wordType == null || wordType.isEmpty() || wordType.trim().length() == 0) {
            throw new SequenceException("The word type specified is null, or empty");
        }
        // Create a kmer.
        char[] kmer = new char[kmerLength];
        System.arraycopy(rawsequence, sequenceStartOffset, kmer, 0, kmerLength);
        return new KMer(kmer, readerIdx, wordType);
    }

    /**
     * Create a kmer from the character sequence, with the offset and specified Lucene word type
     *
     * @param kmer The kmer in char array format.
     * @param offset The offset into the sequence which this kmer exists.
     * @param wordType The Lucene word type to apply with this kmer.
     * @throws SequenceException If the sequence is null or has no length<br>The reader index (for lucene) is
     * negative<br>The word type for lucene is invalid.
     */
    public KMer(char[] kmer, int offset, String wordType) throws SequenceException {
        super();
        if (kmer == null || kmer.length == 0) {
            throw new SequenceException("The raw kmer passed in is either NULL or is empty.");
        }
        if (offset < 0) {
            throw new SequenceException("The reader index for lucene is less than 0?");
        }
        if (offset + kmer.length < 0) {
            throw new SequenceException("IllegalStateException: The reader index plus kmer length leads to integer overflow.");
        }
        if (wordType == null || wordType.isEmpty() || wordType.trim().length() == 0) {
            throw new SequenceException("The word type specified is null, or empty");
        }
        this.kmer = kmer;
        this.offset = offset;
        this.type = wordType;
    }

    /**
     * Get the kmer as a string.
     *
     * @return The kmer as a string.
     */
    public String getString() {
        return new String(getKMer(), 0, getLength());
    }

    /**
     * Get a string representation of this object.
     *
     * @return A description of this object.
     */
    @Override
    public String toString() {
        return getString();
    }

    /**
     * Get the length of the kmer.
     *
     * @return The length of the kmer.
     */
    public int getLength() {
        return kmer.length;
    }

    /**
     * Get the kmer as a raw character array.
     *
     * @return The kmer as a raw character array.
     */
    public char[] getKMer() {
        return kmer.clone();
    }

    /**
     * Get the offset to which this kmer exists in the raw sequence.
     *
     * @return The offset.
     */
    public int getOffset() {
        return offset;
    }

    /**
     * Get the end offset to which is kmer exists in the raw sequence.
     *
     * @return The end offset
     */
    public int getEndOffset() {
        return getOffset() + getLength();
    }

    /**
     * Get the Lucene word type.
     *
     * @return The Lucene word type.
     */
    public String getType() {
        return type;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Arrays.hashCode(this.kmer);
        hash = 59 * hash + this.offset;
        hash = 59 * hash + Objects.hashCode(this.type);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final KMer other = (KMer) obj;
        if (!Arrays.equals(this.kmer, other.kmer)) {
            return false;
        }
        if (this.offset != other.offset) {
            return false;
        }
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        return true;
    }
}
