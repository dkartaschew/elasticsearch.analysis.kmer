/*
 * Copyright 2013 Darran Kartaschew.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package au.edu.qut.bioinformatics.analysis;

import au.edu.qut.bioinformatics.exceptions.SequenceException;
import java.io.Reader;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.lucene.analysis.Analyzer;

/**
 * Builds a token stream which can then be analysed.
 *
 * @author Darran Kartaschew
 */
public class KMerAnalyzer extends Analyzer {

    /**
     * The minimum kmer size to create.
     */
    private final int minKmerSize;
    /**
     * The maximum kmer size to create.
     */
    private final int maxKmerSize;

    /**
     * Default constructor for the KMer Analyser.
     */
    public KMerAnalyzer() {
        super();
        minKmerSize = 5;
        maxKmerSize = 20;
    }

    /**
     * Default constructor for the KMer Analyser with kmer size definition.
     *
     * @param minSize The smallest kmer to create.
     * @param maxSize The largest kmer to create.
     */
    public KMerAnalyzer(int minSize, int maxSize) {
        super();
        minKmerSize = minSize;
        maxKmerSize = maxSize;
    }

    @Override
    protected TokenStreamComponents createComponents(String fieldName, Reader reader) {
        try {
            return new TokenStreamComponents(new KMerTokenizer(reader, minKmerSize, maxKmerSize));
        } catch (SequenceException ex) {
            Logger.getLogger(KMerAnalyzer.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
