/*
 * Copyright 2013 Darran Kartaschew.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.elasticsearch.plugin.analysis.kmer;

import org.elasticsearch.common.inject.Module;
import org.elasticsearch.index.analysis.AnalysisModule;
import org.elasticsearch.index.analysis.KMerAnalysisBinderProcessor;
import org.elasticsearch.plugins.AbstractPlugin;

/**
 * kmer analysis plugin for ElasticSearch
 *
 * @author Darran Kartaschew
 */
public class AnalysisKmerPlugin extends AbstractPlugin {

    /**
     * The name of the plugin.
     */
    @Override
    public String name() {
        return "analysis-kmer";
    }

    /**
     * The description of the plugin.
     */
    @Override
    public String description() {
        return "kmer analysis";
    }

    /**
     * Process module and ensure that all bindings are correct.
     * @param module 
     */
    @Override
    public void processModule(Module module) {
        if (module instanceof AnalysisModule) {
            AnalysisModule analysisModule = (AnalysisModule) module;
            analysisModule.addProcessor(new KMerAnalysisBinderProcessor());
        }
    }
}
