/*
 * Copyright 2013 Darran Kartaschew.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.elasticsearch.index.analysis;

import au.edu.qut.bioinformatics.analysis.KMerTokenizer;
import au.edu.qut.bioinformatics.exceptions.SequenceException;
import org.apache.lucene.analysis.Tokenizer;
import org.elasticsearch.common.inject.Inject;
import org.elasticsearch.common.inject.assistedinject.Assisted;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.env.Environment;
import org.elasticsearch.index.Index;
import org.elasticsearch.index.settings.IndexSettings;

import java.io.File;
import java.io.Reader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Tokeniser Factory for KMers.
 *
 * @author Darran Kartaschew
 */
public class KMerTokenizerFactory extends AbstractTokenizerFactory {

    /**
     * The maximum kmer size to create.
     */
    private final String maxSize;
    /**
     * The minimum kmwer size to create.
     */
    private final String minSize;

    @Inject
    public KMerTokenizerFactory(Index index, @IndexSettings Settings indexSettings, Environment env, @Assisted String name, @Assisted Settings settings) {
        super(index, indexSettings, name, settings);
        logger.info(new File(env.configFile(), "kmer").getPath());
        maxSize = settings.get("kmer_MaxSize", "20");
        minSize = settings.get("kmer_MinSize", "5");
    }

    @Override
    public Tokenizer create(Reader reader) {
        logger.info(maxSize);
        logger.info(minSize);

        // Convert the settings (as strings) to their number equivalent. If either fail then set both to zero, ensuring defaults are used.
        int minK;
        int maxK;
        try {
            minK = Integer.parseInt(minSize);
            maxK = Integer.parseInt(maxSize);
        } catch (NumberFormatException ex) {
            Logger.getLogger(KMerTokenizerFactory.class.getName()).log(Level.SEVERE, null, ex);
            minK = 0;
            maxK = 0;
        }
        try {
            // If any of our K values is incorrect, use defaults.
            if ((minK > maxK) || minK < 1 || maxK < 1) {
                return new KMerTokenizer(reader);
            } else {
                return new KMerTokenizer(reader, minK, maxK);
            }
        } catch (SequenceException ex) {
            Logger.getLogger(KMerTokenizerFactory.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }
}
