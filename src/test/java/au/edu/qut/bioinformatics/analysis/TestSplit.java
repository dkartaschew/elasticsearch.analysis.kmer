/*
 * Copyright 2013 Darran Kartaschew.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package au.edu.qut.bioinformatics.analysis;

import au.edu.qut.bioinformatics.exceptions.SequenceException;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.net.URL;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * JUnit tests for testing word splitting.
 *
 * @author darran
 */
public class TestSplit {

    /**
     * Test sequence for file input (10MB file).
     */
    public final static String testSequence = "/ALF000011.gbk";
    
    /**
     * Micro string for testing the word split (7 bases).
     */
    public final String sequenceNano = "tatcaat";
    /**
     * Micro string for testing the word split (21 bases).
     */
    public final String sequenceMicro = "taaaaactgcaggctatcaat";
    /**
     * Short string for testing the word split (40 bases).
     */
    public final String sequenceShort = "taaaaactgcaggctatcaatctgcattaatggttcccacggagattttagctgaacagc";
    /**
     * Medium string for testing the word split (600 bases)
     */
    public final String sequenceMedium = "taaaaactgcaggctatcaatctgcattaatggttcccacggagattttagctgaacagc"
            + "acgctgaaagtttaatacaactatttggtaatacaatgaacgtagctttattaactggtt"
            + "ctgttaaagggaaaaaaagacgacttcttttagagcaattagaaaatggaactatcgatt"
            + "gtttgattggtacacacgccttgattcaagatgatgtagtcttcaataatgttggattag"
            + "tcattacagatgaacagcaccgatttggtgttaaccaacgacaaattctaagagaaaaag"
            + "gtgcaatgacaaatgtattgtttatgacagcgacaccaatccctagaacactcgctattt"
            + "ctgttttcggtgaaatggatgtatcttcaattaaacaattaccaaaggggagaaaaccta"
            + "taaaaacaagttgggccaaacatgaacaatatgatcaagtacttgcacaaatgtcgaatg"
            + "aattaaaaaaaggtagacaagcctatgtcatctgtccattaattgagagctctgagcatt"
            + "tagaagatgtacaaaatgttgtggcactttatgaatccttacaaagtgattatggtaatg"
            + "aaaaagttggattattacatgggaaaatgtctgcagaagataaagatcaagtcatgcaaa"
            + "aattcagtaaacatgaaatagatattttagtttctactactgtagttgaggtaggtgtaa"
            + "atgtacctaatgcaacgtttatgatgatttatgatgcagatcgattcggtttatctacat"
            + "tacatcaattacgtgggcgtgttggtcgtagtgaacatcaaagttattgtgtattaattg"
            + "catctcccaagactgaaacaggtattgaacgtatgaccattatgactcaaactactgatg";
    /**
     * Long string for testing the word split (1440 bases)
     */
    public final String sequenceLong = "taaaaactgcaggctatcaatctgcattaatggttcccacggagattttagctgaacagc"
            + "acgctgaaagtttaatacaactatttggtaatacaatgaacgtagctttattaactggtt"
            + "ctgttaaagggaaaaaaagacgacttcttttagagcaattagaaaatggaactatcgatt"
            + "gtttgattggtacacacgccttgattcaagatgatgtagtcttcaataatgttggattag"
            + "tcattacagatgaacagcaccgatttggtgttaaccaacgacaaattctaagagaaaaag"
            + "gtgcaatgacaaatgtattgtttatgacagcgacaccaatccctagaacactcgctattt"
            + "ctgttttcggtgaaatggatgtatcttcaattaaacaattaccaaaggggagaaaaccta"
            + "taaaaacaagttgggccaaacatgaacaatatgatcaagtacttgcacaaatgtcgaatg"
            + "aattaaaaaaaggtagacaagcctatgtcatctgtccattaattgagagctctgagcatt"
            + "tagaagatgtacaaaatgttgtggcactttatgaatccttacaaagtgattatggtaatg"
            + "aaaaagttggattattacatgggaaaatgtctgcagaagataaagatcaagtcatgcaaa"
            + "aattcagtaaacatgaaatagatattttagtttctactactgtagttgaggtaggtgtaa"
            + "atgtacctaatgcaacgtttatgatgatttatgatgcagatcgattcggtttatctacat"
            + "tacatcaattacgtgggcgtgttggtcgtagtgaacatcaaagttattgtgtattaattg"
            + "catctcccaagactgaaacaggtattgaacgtatgaccattatgactcaaactactgatg"
            + "gattcgaattaagtgaaagagatttagaaatgagaggtccaggggatttctttggagtaa"
            + "aacaaagcggtcttccagactttcttgtagcaaatgtagtagaagattatcgtatgcttg"
            + "aagttgctagagatgaagcagcagaacttatacaatctggccaatttttcgaacaacaat"
            + "atagtcatttaagagagtttattaaacaaaacttacgccatattcgtttcgattagaaaa"
            + "atattatactattcaaagatatattttgattttattaaaagatacaatcagctgaatata"
            + "gattgatacagtttattgttgagtaatagataaagaaagtgttatgatatttaaggaata"
            + "tttaagacttggtactaaaatgaggggtgagcgtgtgaaattaaagaaaaacgatagacg"
            + "agtagccattaaagaggctatcgaattaaaccccttcatcactgattatgaattgtgtga"
            + "gaagtttgatgtaagtattcaaactatacgtttagatcgcacgcatttaaatattcctga"
            + "gttacgtaaaagaattaaattggtagctgaacaaaattatggacgaattaaatcaataga"
            + "agccaatgaaattataggtgatttgattcaagtgaatcctgatgttagcgcacaatcttt"
            + "aattgaaattacaattgattctgtttttgcaaaaagtgagattgctagagggcacgtctt"
            + "atttgcacaagctaactcattatgtgtagctcttatacataaaccaatagtattgacaca"
            + "tgaaagtcaagttgaatttaaagaaaaagtaaaattaaatgatacagttcgagcagatgc"
            + "ccgtgtcatagatataacagataagcattatattattgaagtgaattcttatgtttcaga"
            + "tatgttagtttttaaaggtaaattcaaaatgtactatacaagtgaggatgaatgaaatgg"
            + "ttaaaatcgcagtagatatgatgggcggggatgatgcgcctggtattgtattagatgcag"
            + "ttaaaaaagctgtcgaggactttaaagatttggaaattattctttttggtgacgaatcac"
            + "aatacaatttaagtcatgagcgaatagagtttagacattgtactgaaaagattgaaatgg"
            + "aagatgaaccagtacgtgcaattaaacgtaaaaaagatagctcgatggttaagatggctg"
            + "aggcggtaaaatcaggagaagctgatggttgtgtatcagctggaaatacaggagctttaa"
            + "tgtcggctggattatttattgttggacgtattaaaggcgtagcgagacctgcattagttg";

    /**
     * Test basic nano sequence, and get the length of all kmer's extracted.
     */
    @Test
    public void testNanoSequence() throws IOException, SequenceException {
        String result = getKMerList(new StringReader(sequenceNano), "\n");
        assertEquals(result.length(), 39);
    }

    /**
     * Test basic nano sequence, and get the length of all kmer's extracted.
     */
    @Test
    public void testNanoSequenceKMerCount() throws IOException, SequenceException {
        int kmerCount = 0;
        Sequence sequence = new Sequence(new StringReader(sequenceNano));
        while (sequence.next() != null) {
            kmerCount++;
        }
        assertEquals(kmerCount, 6);
    }

    /**
     * Test basic micro sequence, and get the length of all kmer's extracted.
     */
    @Test
    public void testMicroSequenceKMerCount() throws IOException, SequenceException {
        int kmerCount = 0;
        Sequence sequence = new Sequence(new StringReader(sequenceMicro));
        while (sequence.next() != null) {
            kmerCount++;
        }
        assertEquals(kmerCount, 152);
    }

    /**
     * Test basic short sequence, and get the length of all kmer's extracted.
     */
    @Test
    public void testShortSequenceKMerCount() throws IOException, SequenceException {
        int kmerCount = 0;
        Sequence sequence = new Sequence(new StringReader(sequenceShort));
        while (sequence.next() != null) {
            kmerCount++;
        }
        assertEquals(kmerCount, 776);
    }

    /**
     * Test basic medium sequence, and get the length of all kmer's extracted.
     */
    @Test
    public void testMediumSequenceKMerCount() throws IOException, SequenceException {
        int kmerCount = 0;
        Sequence sequence = new Sequence(new StringReader(sequenceMedium));
        while (sequence.next() != null) {
            kmerCount++;
        }
        assertEquals(kmerCount, 14216);
    }

    /**
     * Test basic long sequence, and get the length of all kmer's extracted.
     */
    @Test
    public void testLongSequenceKMerCount() throws IOException, SequenceException {
        int kmerCount = 0;
        Sequence sequence = new Sequence(new StringReader(sequenceLong));
        while (sequence.next() != null) {
            kmerCount++;
        }
        assertEquals(kmerCount, 35336);
    }
    
    /**
     * Test basic long sequence, and get the length of all kmer's extracted.
     */
    @Test
    public void test10MBSequenceKMerCount() throws IOException, SequenceException {
        int kmerCount = 0;
        URL filename = getClass().getResource(testSequence);
        Sequence sequence = new Sequence(new FileReader(new File(filename.getFile())));
        while (sequence.next() != null) {
            kmerCount++;
        }
        assertEquals(kmerCount, 180889048);
    }

    /**
     * Test constructor that should fail on kmer lengths being zero.
     */
    @Test(expected = SequenceException.class)
    public void testBadSequenceConstructor() throws SequenceException {
        Sequence sequence = new Sequence(new StringReader(sequenceLong), 0, 0);
        System.out.println(sequence.toString());
    }

    /**
     * Test constructor that should fail on kmer lengths being negative.
     */
    @Test(expected = SequenceException.class)
    public void testBadSequenceConstructor2() throws SequenceException {
        Sequence sequence = new Sequence(new StringReader(sequenceLong), -1, 0);
        System.out.println(sequence.toString());
    }

    /**
     * Test constructor that should fail on kmer lengths being negative.
     */
    @Test(expected = SequenceException.class)
    public void testBadSequenceConstructor3() throws SequenceException {
        Sequence sequence = new Sequence(new StringReader(sequenceLong), 0, -1);
        System.out.println(sequence.toString());
    }

    /**
     * Test constructor that should fail on kmer lengths with max being smaller than min.
     */
    @Test(expected = SequenceException.class)
    public void testBadSequenceConstructor4() throws SequenceException {
        Sequence sequence = new Sequence(new StringReader(sequenceLong), 5, 4);
        System.out.println(sequence.toString());
    }

    /**
     * Test basic nano sequence, and confirm the kmers are as expected.
     */
    @Test
    public void testMicroSequenceComplete20Mer() throws IOException, SequenceException {
        Sequence sequence = new Sequence(new StringReader(sequenceMicro), 20, 20);
        KMer kmer = sequence.next();
        assertNotNull(kmer);
        assertEquals(kmer.toString(), "taaaaactgcaggctatcaa");
        kmer = sequence.next();
        assertNotNull(kmer);
        assertEquals(kmer.toString(), "aaaaactgcaggctatcaat");
        kmer = sequence.next();
        assertNull(kmer);
    }

    /**
     * Test basic Micro sequence, and confirm the kmers are as expected.
     */
    @Test
    public void testMicroSequenceComplete57Mer() throws IOException, SequenceException {
        String[] kmers = new String[]{
            "taaaa", "taaaaa", "taaaaac",
            "aaaaa", "aaaaac", "aaaaact",
            "aaaac", "aaaact", "aaaactg",
            "aaact", "aaactg", "aaactgc",
            "aactg", "aactgc", "aactgca",
            "actgc", "actgca", "actgcag",
            "ctgca", "ctgcag", "ctgcagg",
            "tgcag", "tgcagg", "tgcaggc",
            "gcagg", "gcaggc", "gcaggct",
            "caggc", "caggct", "caggcta",
            "aggct", "aggcta", "aggctat",
            "ggcta", "ggctat", "ggctatc",
            "gctat", "gctatc", "gctatca",
            "ctatc", "ctatca", "ctatcaa",
            "tatca", "tatcaa", "tatcaat",
            "atcaa", "atcaat", "tcaat"};

        Sequence sequence = new Sequence(new StringReader(sequenceMicro), 5, 7);
        for(String term : kmers){
            KMer kmer = sequence.next();
            assertNotNull(kmer);
            assertEquals(kmer.toString(), term);
        }
        KMer kmer = sequence.next();
        assertNull(kmer);
    }
    
    /**
     * Test basic Micro sequence, and confirm the kmers are as expected.
     */
    @Test
    public void testMicroSequenceComplete1Mer() throws IOException, SequenceException {
        String[] kmers = new String[]{"t","a","t","c","a","a","t"};

        // Why oh why only have a mer of 1?
        Sequence sequence = new Sequence(new StringReader(sequenceNano), 1, 1);
        
        for(String term : kmers){
            KMer kmer = sequence.next();
            assertNotNull(kmer);
            assertEquals(kmer.toString(), term);
        }
        KMer kmer = sequence.next();
        assertNull(kmer);
    }

    /**
     * Get a string that includes all kmers for the given sequence.
     *
     * @param input The reader input, in which the sequence is feed through.
     * @param wordSpilt The delimiter to use to separate the kmers.
     * @return A string with the kmers listed.
     * @throws IOException
     */
    public String getKMerList(Reader input, String wordSpilt) throws IOException, SequenceException {
        StringBuilder sb = new StringBuilder();
        Sequence sequence = new Sequence(input);
        KMer word;
        boolean first = true;
        while ((word = sequence.next()) != null) {
            if (!first) {
                sb.append(wordSpilt);
            }
            sb.append(word.getString());
            first = false;
        }
        return sb.toString();
    }
}
