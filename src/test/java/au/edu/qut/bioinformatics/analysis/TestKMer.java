/*
 * Copyright 2013 Darran Kartaschew.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package au.edu.qut.bioinformatics.analysis;

import au.edu.qut.bioinformatics.exceptions.SequenceException;
import java.util.Arrays;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * JUnit tests for testing KMer creation
 *
 * @author Darran Kartaschew
 */
public class TestKMer {
    
    /**
     * Micro string for testing the word split (7 bases).
     */
    public final String sequenceNano = "tatcaat";
    /**
     * Micro string for testing the word split (21 bases).
     */
    public final String sequenceMicro = "taaaaactgcaggctatcaat";
    
    /**
     * Test basic nano sequence, and get the length of all kmer's extracted.
     */
    @Test
    public void testNanoSequence() throws SequenceException {
        String result = KMer.create(sequenceNano.toCharArray(), 0, sequenceNano.length(), 0, KMer.TYPE_WORD).getString();
        assertEquals(result, "tatcaat");
    }
    
    /**
     * Test basic nano sequence, and get the length of all kmer's extracted.
     */
    @Test
    public void testNanoSequence2() throws SequenceException {
        String result = KMer.create(sequenceNano.toCharArray(), 1, sequenceNano.length()-1, 0, KMer.TYPE_WORD).getString();
        assertEquals(result, "atcaat");
    }
    
    /**
     * Test basic Micro sequence, and get the length of all kmer's extracted.
     */
    @Test
    public void testMicroSequence() throws SequenceException {
        String result = KMer.create(sequenceMicro.toCharArray(), 1, sequenceMicro.length()-5, 0, KMer.TYPE_WORD).getString();
        assertEquals(result, "aaaaactgcaggctat");
    }
    
    /**
     * Test basic Micro sequence fail conditions (null sequence)
     */
    @Test(expected = SequenceException.class)
    public void testMicroSequenceFailNoSequence() throws SequenceException {
        String result = KMer.create(null, 1, sequenceMicro.length()-5, 0, KMer.TYPE_WORD).getString();
    }
    
    /**
     * Test basic Micro sequence fail conditions (zero length sequence)
     */
    @Test(expected = SequenceException.class)
    public void testMicroSequenceFailNilSequence() throws SequenceException {
        String result = KMer.create(new char[0], 1, sequenceMicro.length()-5, 0, KMer.TYPE_WORD).getString();
    }
    
    /**
     * Test basic Micro sequence fail conditions (kmer bigger than sequence)
     */
    @Test(expected = SequenceException.class)
    public void testMicroSequenceFailTooSmallSequence() throws SequenceException {
        String result = KMer.create(sequenceMicro.toCharArray(), 0, sequenceMicro.length()+1, 0, KMer.TYPE_WORD).getString();
    }
    
    /**
     * Test basic Micro sequence fail conditions (kmer bigger than sequence)
     */
    @Test(expected = SequenceException.class)
    public void testMicroSequenceFailTooSmallSequence2() throws SequenceException {
        String result = KMer.create(sequenceMicro.toCharArray(), 0, Integer.MAX_VALUE, 0, KMer.TYPE_WORD).getString();
    }
    
    /**
     * Test basic Micro sequence fail conditions (neg offset)
     */
    @Test(expected = SequenceException.class)
    public void testMicroSequenceFailNegStartOffset() throws SequenceException {
        String result = KMer.create(sequenceMicro.toCharArray(), -1, sequenceMicro.length(), 0, KMer.TYPE_WORD).getString();
    }
    
    /**
     * Test basic Micro sequence fail conditions (neg length)
     */
    @Test(expected = SequenceException.class)
    public void testMicroSequenceFailNegLength() throws SequenceException {
        String result = KMer.create(sequenceMicro.toCharArray(), 0, -1, 0, KMer.TYPE_WORD).getString();
    }
    
    /**
     * Test basic Micro sequence fail conditions (zero length)
     */
    @Test(expected = SequenceException.class)
    public void testMicroSequenceFailZeroLength() throws SequenceException {
        String result = KMer.create(sequenceMicro.toCharArray(), 0, 0, 0, KMer.TYPE_WORD).getString();
    }
    
    /**
     * Test basic Micro sequence fail conditions (zero length)
     */
    @Test(expected = SequenceException.class)
    public void testMicroSequenceFailNegIndex() throws SequenceException {
        String result = KMer.create(sequenceMicro.toCharArray(), 0, 0, -1, KMer.TYPE_WORD).getString();
    }
    
    /**
     * Test basic Micro sequence fail conditions (zero length)
     */
    @Test(expected = SequenceException.class)
    public void testMicroSequenceFailNegIndex2() throws SequenceException {
        String result = KMer.create(sequenceMicro.toCharArray(), 0, 0, Integer.MIN_VALUE, KMer.TYPE_WORD).getString();
    }
    
    /**
     * Test basic Micro sequence fail conditions (zero length)
     */
    @Test(expected = SequenceException.class)
    public void testMicroSequenceFailInvalidType() throws SequenceException {
        String result = KMer.create(sequenceMicro.toCharArray(), 0, 0, Integer.MIN_VALUE, null).getString();
    }
    
    /**
     * Test basic Micro sequence fail conditions (zero length)
     */
    @Test(expected = SequenceException.class)
    public void testMicroSequenceFailInvalidType2() throws SequenceException {
        String result = KMer.create(sequenceMicro.toCharArray(), 0, 0, Integer.MIN_VALUE, " ").getString();
    }
    
    /**
     * Test for direct kmer creation.
     */
    @Test
    public void testDirectSequence() throws SequenceException {
        KMer result = new KMer(sequenceMicro.toCharArray(), 0, KMer.TYPE_WORD);
        assertEquals(result.getString(), sequenceMicro);
        assertEquals(result.getLength(), sequenceMicro.length());
        assertEquals(result.getType(), KMer.TYPE_WORD);
        assertEquals(result.getEndOffset(), sequenceMicro.length());
        assertTrue(Arrays.equals(result.getKMer(), sequenceMicro.toCharArray()));
    }
    
    /**
     * Test for direct kmer creation. (fail on null sequence)
     */
    @Test(expected = SequenceException.class)
    public void testDirectSequenceFailNullSequence() throws SequenceException {
        KMer result = new KMer(null, 0, KMer.TYPE_WORD);
    }
    
    /**
     * Test for direct kmer creation. (fail on nil sequence)
     */
    @Test(expected = SequenceException.class)
    public void testDirectSequenceFailNilSequence() throws SequenceException {
        KMer result = new KMer(new char[0], 0, KMer.TYPE_WORD);
    }
    
    /**
     * Test for direct kmer creation. (fail on negative offset for index.)
     */
    @Test(expected = SequenceException.class)
    public void testDirectSequenceFailNegOffset() throws SequenceException {
        KMer result = new KMer(sequenceMicro.toCharArray(), -1, KMer.TYPE_WORD);
    }
    
    /**
     * Test for direct kmer creation. (fail on negative offset)
     */
    @Test(expected = SequenceException.class)
    public void testDirectSequenceFailNegOffset2() throws SequenceException {
        KMer result = new KMer(sequenceMicro.toCharArray(), Integer.MIN_VALUE, KMer.TYPE_WORD);
    }
    
    /**
     * Test for direct kmer creation. (fail on integer overflow for endoffset)
     */
    @Test(expected = SequenceException.class)
    public void testDirectSequenceFailHugeOffsetOverflow() throws SequenceException {
        KMer result = new KMer(sequenceMicro.toCharArray(), Integer.MAX_VALUE, KMer.TYPE_WORD);
    }
    
    /**
     * Test for direct kmer creation. (fail on null type)
     */
    @Test(expected = SequenceException.class)
    public void testDirectSequenceNULLType() throws SequenceException {
        KMer result = new KMer(sequenceMicro.toCharArray(), 0, null);
    }
    
    /**
     * Test for direct kmer creation. (fail on empty type)
     */
    @Test(expected = SequenceException.class)
    public void testDirectSequenceNilType() throws SequenceException {
        KMer result = new KMer(sequenceMicro.toCharArray(), 0, "  ");
    }
    
    /**
     * Test basic nano sequence duplicates
     */
    @Test
    public void testNanoSequenceEquals() throws SequenceException {
        KMer result1 = new KMer(sequenceNano.toCharArray(), 0, KMer.TYPE_WORD);
        KMer result2 = new KMer(sequenceNano.toCharArray(), 0, KMer.TYPE_WORD);
        assertEquals(result1, result2);
    }
    
    /**
     * Test basic nano sequence duplicates
     */
    @Test
    public void testNanoSequenceEquals2() throws SequenceException {
        KMer result1 = new KMer(sequenceNano.toCharArray(), 0, KMer.TYPE_WORD);
        KMer result2 = new KMer(sequenceNano.toCharArray(), 1, KMer.TYPE_WORD);
        assertNotEquals(result1, result2);
    }
    
    /**
     * Test basic nano sequence duplicates
     */
    @Test
    public void testNanoSequenceEquals3() throws SequenceException {
        KMer result1 = new KMer(sequenceNano.toCharArray(), 0, KMer.TYPE_WORD);
        KMer result2 = new KMer(sequenceNano.toCharArray(), 0, "phrase");
        assertNotEquals(result1, result2);
    }
    
    /**
     * Test basic nano sequence duplicates
     */
    @Test
    public void testNanoSequenceEquals4() throws SequenceException {
        KMer result1 = new KMer(sequenceNano.toCharArray(), 0, KMer.TYPE_WORD);
        KMer result2 = new KMer(sequenceMicro.toCharArray(), 0, KMer.TYPE_WORD);
        assertNotEquals(result1, result2);
    }
    
    /**
     * Test basic nano sequence duplicates
     */
    @Test
    public void testNanoSequenceHash() throws SequenceException {
        KMer result1 = new KMer(sequenceNano.toCharArray(), 0, KMer.TYPE_WORD);
        assertEquals(result1.hashCode(), 724998558);
    }
    
    /**
     * Test basic nano sequence duplicates
     */
    @Test
    public void testNanoSequenceStaticTests() throws SequenceException {
        KMer result1 = new KMer(sequenceNano.toCharArray(), 0, KMer.TYPE_WORD);
        KMer result2 = new KMer(sequenceMicro.toCharArray(), 1, KMer.TYPE_WORD);
        assertNotEquals(result1, result2);
        assertNotEquals(result1.getOffset(), result2.getOffset());
        assertNotEquals(result1.getEndOffset(), result2.getEndOffset());
        assertNotEquals(result1.getLength(), result2.getLength());
        assertNotEquals(result1.toString(), result2.toString());
        assertEquals(result1.getType(), result2.getType());
    }
}
