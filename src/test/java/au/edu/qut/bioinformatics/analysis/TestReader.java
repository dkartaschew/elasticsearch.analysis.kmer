/*
 * Copyright 2013 darran.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package au.edu.qut.bioinformatics.analysis;

import au.edu.qut.bioinformatics.exceptions.SequenceException;
import java.io.IOException;
import java.io.StringReader;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author darran
 */
public class TestReader {

    /**
     * The default buffer size.
     */
    public final int BUFFER_SIZE = 20;
    /**
     * The default buffer to use during testing.
     */
    public char[] buffer;
    /**
     * Test sequence for file input (10MB file).
     */
    public final static String testSequence = "/ALF000011.gbk";
    /**
     * Micro string for testing the word split (7 bases).
     */
    public final String sequenceNano = "tatcaat";
    /**
     * Micro string for testing the word split (21 bases).
     */
    public final String sequenceMicro = "taaaaactgcaggctatcaat";
    /**
     * Short string for testing the word split (40 bases).
     */
    public final String sequenceShort = "taaaaactgcaggctatcaatctgcattaatggttcccacggagattttagctgaacagc";
    /**
     * Medium string for testing the word split (600 bases)
     */
    public final String sequenceMedium = "taaaaactgcaggctatcaatctgcattaatggttcccacggagattttagctgaacagc"
            + "acgctgaaagtttaatacaactatttggtaatacaatgaacgtagctttattaactggtt"
            + "ctgttaaagggaaaaaaagacgacttcttttagagcaattagaaaatggaactatcgatt"
            + "gtttgattggtacacacgccttgattcaagatgatgtagtcttcaataatgttggattag"
            + "tcattacagatgaacagcaccgatttggtgttaaccaacgacaaattctaagagaaaaag"
            + "gtgcaatgacaaatgtattgtttatgacagcgacaccaatccctagaacactcgctattt"
            + "ctgttttcggtgaaatggatgtatcttcaattaaacaattaccaaaggggagaaaaccta"
            + "taaaaacaagttgggccaaacatgaacaatatgatcaagtacttgcacaaatgtcgaatg"
            + "aattaaaaaaaggtagacaagcctatgtcatctgtccattaattgagagctctgagcatt"
            + "tagaagatgtacaaaatgttgtggcactttatgaatccttacaaagtgattatggtaatg"
            + "aaaaagttggattattacatgggaaaatgtctgcagaagataaagatcaagtcatgcaaa"
            + "aattcagtaaacatgaaatagatattttagtttctactactgtagttgaggtaggtgtaa"
            + "atgtacctaatgcaacgtttatgatgatttatgatgcagatcgattcggtttatctacat"
            + "tacatcaattacgtgggcgtgttggtcgtagtgaacatcaaagttattgtgtattaattg"
            + "catctcccaagactgaaacaggtattgaacgtatgaccattatgactcaaactactgatg";

    @Before
    public void setup() {
        buffer = new char[BUFFER_SIZE];
    }

    /**
     * Test basic reader operation.
     */
    @Test
    public void testReaderMicro() throws IOException {
        PeekableBufferedReader reader = new PeekableBufferedReader(new StringReader(sequenceNano), BUFFER_SIZE);
        int read = reader.read(buffer, BUFFER_SIZE);
        assertEquals(read, sequenceNano.length());
    }

    /**
     * Test basic reader operation.
     */
    @Test
    public void testReaderMicro5() throws IOException {
        PeekableBufferedReader reader = new PeekableBufferedReader(new StringReader(sequenceNano), BUFFER_SIZE);
        int read = reader.read(buffer, 5);
        assertEquals(read, 5);
        reader.seek(1);
        read = reader.read(buffer, 7);
        assertEquals(read, 6);
        reader.seek(1);
        read = reader.read(buffer, 7);
        assertEquals(read, 5);
    }

    /**
     * Test basic Micro sequence, and confirm the kmers are as expected.
     */
    @Test
    public void testMicroReaderComplete1Mer() throws IOException, SequenceException {
        char[] kmers = new char[]{'t', 'a', 't', 'c', 'a', 'a', 't'};

        // Why oh why only have a mer of 1?
        PeekableBufferedReader reader = new PeekableBufferedReader((new StringReader((sequenceNano))), 1);

        for (char term : kmers) {
            int read = reader.read(buffer, 1);
            assertEquals(read, 1);
            reader.seek(1);
            assertEquals(buffer[0], term);
        }
        int read = reader.read(buffer, 1);
        assertEquals(read, -1);
    }

    /**
     * Test basic nano sequence, and get the length of all kmer's extracted.
     */
    @Test
    public void testNanoSequenceKMerCount() throws IOException, SequenceException {
        int kmerCount = 0;
        Sequence sequence = new Sequence(new StringReader(sequenceNano));
        while (sequence.next() != null) {
            kmerCount++;
        }
        assertEquals(kmerCount, 6);
    }

    /**
     * Test basic Micro sequence, and confirm the kmers are as expected.
     */
    @Test
    public void testMicroSequenceComplete1Mer() throws IOException, SequenceException {
        String[] kmers = new String[]{"t", "a", "t", "c", "a", "a", "t"};

        // Why oh why only have a mer of 1?
        Sequence sequence = new Sequence(new StringReader(sequenceNano), 1, 1);

        for (String term : kmers) {
            KMer kmer = sequence.next();
            assertNotNull(kmer);
            assertEquals(kmer.toString(), term);
        }
        KMer kmer = sequence.next();
        assertNull(kmer);
    }

    /**
     * Test basic Micro sequence, and confirm the kmers are as expected.
     */
    @Test
    public void testMicroSequenceComplete57Mer() throws IOException, SequenceException {
        String[] kmers = new String[]{
            "taaaa", "taaaaa", "taaaaac",
            "aaaaa", "aaaaac", "aaaaact",
            "aaaac", "aaaact", "aaaactg",
            "aaact", "aaactg", "aaactgc",
            "aactg", "aactgc", "aactgca",
            "actgc", "actgca", "actgcag",
            "ctgca", "ctgcag", "ctgcagg",
            "tgcag", "tgcagg", "tgcaggc",
            "gcagg", "gcaggc", "gcaggct",
            "caggc", "caggct", "caggcta",
            "aggct", "aggcta", "aggctat",
            "ggcta", "ggctat", "ggctatc",
            "gctat", "gctatc", "gctatca",
            "ctatc", "ctatca", "ctatcaa",
            "tatca", "tatcaa", "tatcaat",
            "atcaa", "atcaat", "tcaat"};

        Sequence sequence = new Sequence(new StringReader(sequenceMicro), 5, 7);
        for (String term : kmers) {
            KMer kmer = sequence.next();
            assertNotNull(kmer);
            assertEquals(kmer.toString(), term);
        }
        KMer kmer = sequence.next();
        assertNull(kmer);
    }

    /*
     * The following test for failure conditions of the reader.
     */
    /**
     * Test for null reader input on constructor.
     */
    @Test(expected = IOException.class)
    public void testBadConstructor() throws IOException {
        PeekableBufferedReader reader = new PeekableBufferedReader(null, BUFFER_SIZE);
    }

    /**
     * Test for zero size buffer
     */
    @Test(expected = IOException.class)
    public void testBadConstructorBufferSize() throws IOException {
        PeekableBufferedReader reader = new PeekableBufferedReader(new StringReader(sequenceMedium), 0);
    }

    /**
     * Test for neg size buffer
     */
    @Test(expected = IOException.class)
    public void testBadConstructorBufferSize2() throws IOException {
        PeekableBufferedReader reader = new PeekableBufferedReader(new StringReader(sequenceMedium), -1);
    }

    /**
     * Test for neg size buffer
     */
    @Test(expected = IOException.class)
    public void testBadConstructorBufferSize3() throws IOException {
        PeekableBufferedReader reader = new PeekableBufferedReader(new StringReader(sequenceMedium), Integer.MIN_VALUE);
    }

    /**
     * Test for neg size read
     */
    @Test(expected = IOException.class)
    public void testNegReadSize() throws IOException {
        PeekableBufferedReader reader = new PeekableBufferedReader(new StringReader(sequenceMedium), BUFFER_SIZE);
        int read = reader.read(buffer, -1);
    }

    /**
     * Test for zero size read
     */
    @Test
    public void testZeroReadSize() throws IOException {
        PeekableBufferedReader reader = new PeekableBufferedReader(new StringReader(sequenceMedium), BUFFER_SIZE);
        int read = reader.read(buffer, 0);
        assertEquals(read, 0);
    }

    /**
     * Test for null buffer
     */
    @Test(expected = IOException.class)
    public void testNullBufferRead() throws IOException {
        PeekableBufferedReader reader = new PeekableBufferedReader(new StringReader(sequenceMedium), BUFFER_SIZE);
        int read = reader.read(null, 10);
    }

    /**
     * Test for large read
     */
    @Test(expected = IOException.class)
    public void testLargeBufferRead() throws IOException {
        PeekableBufferedReader reader = new PeekableBufferedReader(new StringReader(sequenceMedium), BUFFER_SIZE);
        int read = reader.read(buffer, reader.getBufferSize() + 1);
    }

    /**
     * Test for large read when the cache buffer is smaller than the read
     * request.
     */
    @Test(expected = IOException.class)
    public void testLargeBufferRead2() throws IOException {
        PeekableBufferedReader reader = new PeekableBufferedReader(new StringReader(sequenceMedium), 10);
        int read = reader.read(buffer, reader.getBufferSize() + 1);
    }
}
