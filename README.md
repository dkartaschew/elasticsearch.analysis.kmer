kmer Analysis for ElasticSearch
==================================
----
The kmer analyser is to be used with DNA sequencing and is used to extract and index kmers from a DNA sequence.

The plugin includes a `kmer` analyzer and a `kmer` tokenizer.

Install
-------------

* run **$ mvn clean package** to build.
* copy **target\elasticsearch-analysis-kmer-1.0.0.jar** to **$ES_HOME\plugins\analysis-kmer**
* copy **config\mappings\documents** to **$ES_HOME\config\mappings**
* update **$ES_HOME\config\elasticsearch.yml** with configuration below.

Analysis Configuration (elasticsearch.yml)
-------------

    index:
      analysis:
        analyzer:
          kmer:
              alias: [kmer_analyzer]
              type: org.elasticsearch.plugin.analysis.KMerAnalyzerProvider

additional parameters that can be used to customize the kmer tokenizer. (Place this in **config\kmer\config.yml**).


    index:
      analysis:
        tokenizer:
          kmer_size:
              type: kmer
              kmer_MaxSize: "kmerSize"
              kmer_MinSize: "kmerSize"

Mapping Configuration
-------------

Mapping Configuration is done automatically via the **config\mappings\documents**. All fields in the **document** index are treated as regular text except for the **contents** field which will be tokenised by the **kmer** analyser.
